const $ = require('jquery');
require('bootstrap/dist/js/bootstrap.bundle');

require('./images');
require('./exercises');

const ClipboardJS = require('clipboard');

new ClipboardJS('.btn', {
    target: function(trigger) {
        return trigger.previousElementSibling;
    }
});