const $ = require('jquery');

const $list = $('#exercises-list');
const $content = $('#exercises-content');

const readExerciese = function () {
    $content.html($list.find('option:selected').data('content'));
};

$list.change(readExerciese);
readExerciese();
