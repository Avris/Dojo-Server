var Encore = require('@symfony/webpack-encore');
var requireAssetsHelper = require('encore-require-assets-helper');

requireAssetsHelper(
    './assets/images/**/*.{jpg,jpeg,png,gif,svg,ico}',
    './assets/js/images.js',
    './assets/',
    '../'
);

Encore
    .setOutputPath('public/assets/')
    .setPublicPath('/assets')
    .cleanupOutputBeforeBuild()
    .addEntry('app', './assets/js/app.js')
    .addStyleEntry('global', './assets/scss/build.scss')
    .enableSassLoader()
    .autoProvidejQuery()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
;

module.exports = Encore.getWebpackConfig();
