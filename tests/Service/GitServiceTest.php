<?php

namespace App\Service;

use Avris\Bag\Bag;
use Avris\Micrus\Exception\Http\BadRequestHttpException;
use PHPUnit\Framework\TestCase;

final class GitServiceTest extends TestCase
{
    /** @var GitService */
    private $git;

    public function setUp()
    {
        $this->git = new GitService();
    }

    public function testFindRepoInWebhookPayloadFound()
    {
        $repo = 'git@gitlab.com:Avris/Micrus-Demo.git';

        $payload = [
            'foo' => 'bar',
            'project' => [
                'git_ssh_url' => $repo,
                'lorem' => 'ipsum',
            ]
        ];

        $this->assertEquals($repo, $this->git->findRepoInWebhookPayload(new Bag($payload)));
    }

    public function testFindRepoInWebhookPayloadNotFound()
    {
        $payload = [
            'foo' => 'bar',
            'project' => [
                'lorem' => 'ipsum',
            ]
        ];

        $this->expectException(BadRequestHttpException::class);
        $this->git->findRepoInWebhookPayload(new Bag($payload));
    }

    /**
     * @dataProvider normaliseProvider
     */
    public function testNormaliseRepo(string $repoUrl, string $expectedNorm)
    {
        $this->assertEquals($expectedNorm, $this->git->normaliseRepo($repoUrl));
    }

    public function normaliseProvider()
    {
        $norm1 = 'github.com/avris/micrus-demo';
        yield ['https://github.com/avris/micrus-demo', $norm1];
        yield ['git://github.com/avris/micrus-demo.git', $norm1];
        yield ['git@github.com:avris/micrus-demo.git', $norm1];
        yield ['https://github.com/avris/micrus-demo.git', $norm1];

        $norm2 = 'gitlab.com/Avris/Micrus';
        yield ['http://gitlab.com/Avris/Micrus', $norm2];
        yield ['git@gitlab.com:Avris/Micrus.git', $norm2];
        yield ['http://gitlab.com/Avris/Micrus.git', $norm2];

        $norm3 = 'gitlab.haufedev.systems/semigator/misc/coding-dojo';
        yield ['https://gitlab.haufedev.systems/semigator/misc/coding-dojo.git', $norm3];
        yield ['ssh://git@gitlab.haufedev.systems:2222/semigator/misc/coding-dojo.git', $norm3];
    }
}
