# Avris Dojo Server

See: [dojo.avris.it](https://dojo.avris.it)

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
