<?php
namespace App\Controller;

use App\Service\ExcercisesService;
use App\Service\GitService;
use App\Socket\SocketClient;
use Avris\Bag\Bag;
use Avris\Container\ContainerInterface;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Response\Response;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Annotations\Annotation as M;
use Avris\Micrus\Controller\Controller;

final class HomeController extends Controller
{
    /** @var GitService */
    private $git;

    /** @var SocketClient */
    private $client;

    /** @var ExcercisesService */
    private $excercises;

    public function __construct(
        ContainerInterface $container,
        GitService $git,
        SocketClient $client,
        ExcercisesService $excercisesService
    ) {
        parent::__construct($container);
        $this->git = $git;
        $this->client = $client;
        $this->excercises = $excercisesService;
    }

    /**
     * @M\Route("GET /")
     */
    public function homeAction(): ResponseInterface
    {
        return $this->render([
            'exercises' => $this->excercises->get(),
        ]);
    }

    /**
     * @M\Route("GET /ws")
     */
    public function wsAction(): ResponseInterface
    {
        return new Response($this->container->getParameter('SOCKET_HOST'));
    }

    /**
     * @M\Route("POST /push")
     */
    public function pushAction(RequestInterface $request): ResponseInterface
    {
        $payload = new Bag($request->getJsonBody());
        $repo = $this->git->findRepoInWebhookPayload($payload);
        $this->client->trigger($repo);

        return new Response($repo);
    }
}
