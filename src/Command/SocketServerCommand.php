<?php
namespace App\Command;

use App\Socket\SocketServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SocketServerCommand extends Command
{
    /** @var SocketServer */
    private $server;

    public function __construct(SocketServer $server)
    {
        $this->server = $server;
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('socket:server')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->server->configure($output)->run();
    }
}
