<?php
namespace App\Socket;

use Hoa\Websocket\Exception\BadProtocol;
use Hoa\Websocket\Protocol\Hybi00;
use Hoa\Websocket\Protocol\Rfc6455;
use Hoa\Websocket\Server;

/**
 * It's an ugly hotfix for an issue with hoa/websocket
 * https://github.com/hoaproject/Websocket/issues/89
 */
final class WebServer extends Server
{
    protected function doHandshake()
    {
        $connection = $this->getConnection();

        if (true  === $connection->getSocket()->isSecured() &&
            false === $connection->isEncrypted()) {
            $connection->enableEncryption(true, $connection::ENCRYPTION_TLS);
        }

        $buffer = $connection->read(2048);
        if ($buffer === 'G') {                      // <--- hotfix
            $buffer .= $connection->read(2048);
        }
        $request = $this->getRequest();
        $request->parse($buffer);

        // Rfc6455.
        try {
            $rfc6455 = new Rfc6455($connection);
            $rfc6455->doHandshake($request);
            $connection->getCurrentNode()->setProtocolImplementation($rfc6455);
        } catch (BadProtocol $e) {
            unset($rfc6455);

            // Hybi00.
            try {
                $hybi00 = new Hybi00($connection);
                $hybi00->doHandshake($request);
                $connection->getCurrentNode()->setProtocolImplementation($hybi00);
            } catch (BadProtocol $e) {
                unset($hybi00);
                $connection->disconnect();

                throw new BadProtocol('All protocol failed.', 1);
            }
        }

        return;
    }
}
