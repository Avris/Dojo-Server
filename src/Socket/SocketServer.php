<?php
namespace App\Socket;

use App\Service\GitService;
use Hoa\Event\Bucket;
use Hoa\Socket\Server;
//use Hoa\Websocket\Server as WebServer;
use Hoa\Stream\Context;
use Symfony\Component\Console\Output\OutputInterface;

final class SocketServer
{
    /** @var string */
    private $host;

    /** @var WebServer */
    private $server;

    /** @var string|null */
    private $context = null;

    /** @var OutputInterface */
    private $output;

    /** @var GitService */
    private $gitService;

    public function __construct(
        string $envSocketHost,
        string $envSocketCert,
        GitService $gitService
    ) {
        $this->host = $envSocketHost;
        if (!empty($envSocketCert)) {
            $context = Context::getInstance('Default');
            $context->setOptions(['ssl' => ['local_cert' => $envSocketCert]]);
            $this->context = 'Default';
        }
        $this->gitService = $gitService;
    }

    public function configure(OutputInterface $output = null): WebServer
    {
        $this->output = $output;

        $this->server = new WebServer(new Server($this->host, 30, -1, $this->context));
        $this->server->getConnection()->setNodeName(SocketNode::class);
        $this->server->on('open', [$this, 'onOpen']);
        $this->server->on('message', [$this, 'onMessage']);
        $this->server->on('close', [$this, 'onClose']);

        $this->log('Configured host: ' . $this->host);

        return $this->server;
    }

    public function onOpen(Bucket $bucket)
    {
        $this->log('New connection');

        /** @var WebServer $connection */
        $connection = $bucket->getSource();

        $repo = base64_decode(trim($connection->getRequest()->getUrl(), '/')) ?: null;

        /** @var SocketNode $node */
        $node = $connection->getConnection()->getCurrentNode();
        $node->setRepo($repo);

        $this->log(sprintf('Repo: ' . ($repo ?: 'NONE')));
    }

    public function onMessage(Bucket $bucket)
    {
        $this->log('> Message: ' . $bucket->getData()['message']);

        $message = json_decode($bucket->getData()['message'], true);
        if (!isset($message['repo'])) {
            $this->log('Invalid message');

            return;
        }

        $repo = $this->gitService->normaliseRepo($message['repo']);

        $count = 0;

        $bucket->getSource()->broadcastIf(
            function (SocketNode $node) use ($repo, &$count) {
                if (!$node->getRepo()) {
                    return false;
                }

                $send = $this->gitService->normaliseRepo($node->getRepo()) === $repo;
                $count += $send;

                return $send;
            },
            json_encode($message)
        );

        $this->log(
            $count
                ? '< Sent to: ' . $count
                : '< No client interested'
        );
    }

    public function onClose()
    {
        $this->log('Connection closed');
    }

    private function log($messages)
    {
        if ($this->output) {
            $this->output->writeln($messages);
            return;
        }

        echo $messages . "\n";
    }
}
