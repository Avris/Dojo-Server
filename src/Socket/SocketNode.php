<?php
namespace App\Socket;

use Hoa\Websocket\Node;

final class SocketNode extends Node
{
    /** @var string|null */
    private $repo;

    public function getRepo(): ?string
    {
        return $this->repo;
    }

    public function setRepo(?string $repo): self
    {
        $this->repo = $repo;

        return $this;
    }
}
