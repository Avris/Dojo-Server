<?php
namespace App\Socket;

use Avris\Http\Header\HeaderBag;
use Hoa\Socket\Client;
use Hoa\Websocket\Client as WebClient;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;
use ProxyManager\Proxy\LazyLoadingInterface;

final class SocketClient
{
    /** @var WebClient */
    private $client;

    public function __construct(string $envSocketHost, HeaderBag $headers)
    {
        $factory = new LazyLoadingValueHolderFactory();
        // @codingStandardsIgnoreStart
        $initializer = function (&$wrappedObject, LazyLoadingInterface $proxy, $method, array $parameters, &$initializer) use ($envSocketHost, $headers) {
            $initializer = null;
            $wrappedObject = new WebClient(new Client($envSocketHost));
            $this->client->setHost($headers->has('host') ? (string) $headers->get('host') : 'localhost');
            $this->client->connect();

            return true;
        };
        // @codingStandardsIgnoreEnd

        $this->client = $factory->createProxy(WebClient::class, $initializer);
    }

    public function trigger(string $repo)
    {
        $this->client->send(json_encode([
            'repo' => $repo,
        ]));
    }
}
