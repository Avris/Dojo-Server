<?php
namespace App;

use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;
use Avris\Micrus\Tool\Cache\Cacher;
use Avris\Micrus\Tool\Cache\CacherInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

final class App extends AbstractApp implements ModuleInterface
{
    use ModuleTrait;

    protected function registerModules(): iterable
    {
        yield new \Avris\Micrus\Twig\TwigModule;
        yield new \Avris\Micrus\Annotations\AnnotationsModule;
        yield new \Avris\Micrus\GoogleAnalytics\GoogleAnalyticsModule;
        yield $this;
    }

    protected function buildProjectDir(): string
    {
        return dirname(__DIR__);
    }

    protected function buildCacher(): CacherInterface
    {
        $adapter = new FilesystemAdapter('app', 0, $this->configBag->getDir(self::CACHE_DIR));

        if ($this->configBag->isDebug()) {
            return new Cacher($adapter, true);
        }

        return new Cacher($adapter, false);
    }
}
