<?php
namespace App\Service;

use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class TwigGlobals extends AbstractExtension implements GlobalsInterface
{
    public function getGlobals()
    {
        return [
            'title' => 'Avris Dojo',
            'slogan' => 'Practise programming Dojo-style in your own IDE',
            'description' => 'A Coding Dojo is a great way to practise programming, test-driven development, '
                . 'teamwork, pair programming and problem solving. '
                . 'Avris Dojo provides an easy way to synchronise your dojo codebase with your teammates.',
            'keywords' => ['coding', 'dojo', 'programming', 'practise', 'teamwork', 'git', 'sync', 'ide', 'php'],
        ];
    }
}
