<?php
namespace App\Service;

use GuzzleHttp\Client;
use Psr\Cache\CacheItemPoolInterface;

final class ExcercisesService
{
    private const BASE_URL = 'https://api.github.com/repos/cyber-dojo/start-points-exercises/contents/';
    private const CACHE_KEY = 'excercises';

    /** @var string */
    private $githubToken;

    /** @var CacheItemPoolInterface */
    private $cache;

    public function __construct(string $envGithubToken, CacheItemPoolInterface $cache)
    {
        $this->githubToken = $envGithubToken;
        $this->cache = $cache;
    }

    public function get(): array
    {
        $item = $this->cache->getItem(self::CACHE_KEY);
        if (!$item->isHit()) {
            $item->set(iterator_to_array($this->load()));
            $this->cache->save($item);
        }

        return $item->get();
    }

    private function load(): \Traversable
    {
        $client = new Client([
            'base_uri' => self::BASE_URL,
            'headers' => [
                'Authorization' => 'token ' . $this->githubToken,
            ],
        ]);

        foreach (json_decode($client->get('')->getBody(), true) as $file) {
            if ($file['type'] === 'dir' && $file['name'] != '_ci_scripts') {
                $excercise = json_decode($client->get($file['name'] . '/instructions')->getBody(), true);
                yield $file['name'] => base64_decode($excercise['content']);
            }
        }
    }
}
