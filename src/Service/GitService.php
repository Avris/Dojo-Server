<?php
namespace App\Service;

use Avris\Bag\Bag;
use Avris\Micrus\Exception\Http\BadRequestHttpException;

final class GitService
{
    private const REPO_IN_PAYLOAD_OPTIONS = [
        'repository.html_url',
        'repository.git_url',
        'repository.ssh_url',
        'repository.clone_url',
        'project.web_url',
        'project.git_ssh_url',
        'project.git_http_url',
    ];

    private const REPO_URL_REGEX =
        '#^([a-zA-Z0-9-]+://)?' . // protocole
        '([a-zA-Z0-9-_]+@)?' . // user
        '([^/:]+)' . // host
        '(?::[0-9]+)?' . // port
        '[/:]' .
        '([a-zA-Z0-9-_/]+)(\.git)?$#'; // path

    public function findRepoInWebhookPayload(Bag $payload): string
    {
        foreach (self::REPO_IN_PAYLOAD_OPTIONS as $option) {
            if ($value = $payload->getDeep($option)) {
                return $value;
            }
        }

        throw new BadRequestHttpException('Cannot determine a repository based on webhook request body');
    }

    public function normaliseRepo(string $repoUrl): string
    {
        if (!preg_match(self::REPO_URL_REGEX, $repoUrl, $matches)) {
            throw new \InvalidArgumentException(sprintf('Cannot normalise repo %s', $repoUrl));
        }

        list(, , , $host, $path) = $matches;

        return $host . '/' . $path;
    }
}
