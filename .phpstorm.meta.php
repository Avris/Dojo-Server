<?php
namespace PHPSTORM_META {
    override(
        \Avris\Container\ContainerInterface::get(0),
        map([
            '' => '@',
        ])
    );

    override(
        \Avris\Micrus\Controller\Controller::form(0),
        map([
            '' => '@',
        ])
    );

    override(
        \PHPUnit\Framework\TestCase::getMockBuilder(0),
        map([
            '' => '@',
        ])
    );
}
