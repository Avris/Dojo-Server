<?php
require_once __DIR__ . '/../vendor/autoload.php';

mb_internal_encoding('UTF-8');

if (!isset($_SERVER['APP_ENV'])) {
    (new \Avris\Dotenv\Dotenv)->load(__DIR__ . '/../.env');
    umask(0000);
}

$app = new \App\App($_SERVER['APP_ENV'] ?? 'dev', $_SERVER['APP_DEBUG'] ?? false);
$request = \Avris\Http\Request\Request::fromGlobals();
$response = $app->handle($request);
$response->send();
$app->terminate();
